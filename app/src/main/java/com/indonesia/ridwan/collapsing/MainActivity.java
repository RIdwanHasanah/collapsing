package com.indonesia.ridwan.collapsing;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbar;
    SwipeRefreshLayout sw;
    LinearLayout layo;
    TextView tx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // pengaturan dan inisialisasi collapsing toolbar
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbar.setTitle("Ridwan Hasanah");
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.primary_text_light));

        // inisialisasi ImageView
        ImageView header = (ImageView) findViewById(R.id.iv_header);

        sw = (SwipeRefreshLayout) findViewById(R.id.swipe);
        layo= (LinearLayout) findViewById(R.id.l1);
        tx = (TextView) findViewById(R.id.t);

        //mengeset properti warna yang berputar pada Swipe
        sw.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //Handler untuk menjalankan jeda selalama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //Berhenti beputar/refreshing
                        sw.setRefreshing(false);

                        //fungsi - fungsi lain yang di jalankan saat refresh
                        layo.setBackground(ContextCompat.getDrawable(MainActivity.this,R.mipmap.ic_launcher));

                    }
                },5000);
            }
        });

        // mengambil gambar bitmap yang digunakan pada image view
       // Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        // mengekstrak warna dari gambar yang digunakan
       /* Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                mutedColor = palette.getMutedColor(R.attr.colorPrimary);
                collapsingToolbar.setContentScrimColor(mutedColor);
            }
        });*/

    }
}
